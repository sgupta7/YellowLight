#!/bin/sh

# get directory of this script and change to it
dir=$(dirname ${0})
cd ${dir}

# download, extract, and arrange the most up-to-date code
curl https://gitlab.com/sgupta7/YellowLight/repository/master/archive.tar.gz \
> archive.tar.gz
tar -xvf archive.tar.gz
cp -v -fR YellowLight-master-*/* .
rm -rv YellowLight-master-*/

# install pip and virtualenv
sudo easy_install pip
sudo pip install virtualenv

# create a virtual environment and "activate" it
python -m virtualenv venv
source venv/bin/activate

# install requirements
pip install -r requirements.txt
