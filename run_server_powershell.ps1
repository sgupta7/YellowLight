# get directory of this script and change to it
Set-Variable dir $PSScriptRoot
Set-Location $dir

try{
	# start the server
	python src\server.py
}
catch{
	pause
}