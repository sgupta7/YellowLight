# Install on Mac

On a Mac, copy `install.command` from this repo to where you want the app
installed. You may need to give the install command permission to execute.
Double-click on the install command to install the software to the directory
this script was run in.

# Install on Windows

On a windows (7 or later), copy `install_windows` and `install_powershell`
from this repo to where to want the app installed.
Now, open `Powershell` in windows (already installed) and navigate to the directory (can be found by Right-Click->Properties->Location)
containing the above copied files using command `Set-Location <Location of copied files>`
Refer to this for help: `https://docs.microsoft.com/en-us/powershell/scripting/getting-started/cookbooks/managing-current-location?view=powershell-6`

To verify, run command `dir` and it should list the above copied files.
If it doesn't, then you're in the wrong folder.

Run this command `Save-Module -Name 7Zip4Powershell -Path .` The period at the end here is necessary.

Now, you can double-click on the `install_windows` and you're done.

# Run on Mac

Once installed, the `run.command` file should be present. Double-click on this
file. At this point, the server should be running. In a web browser, go to
`localhost:8080/pages/index.html` to see a list of "scenarios" and an option
to start one.

# Run on Windows

Once installed, the `run_windows` and `run_powershell` files should be present.
Double-click on `run_windows` and the server should be running. In a web browser, go to
`localhost:8080/pages/index.html` to see a list of "scenarios" and an option
to start one.

# Configure

Create a file called `scenario.csv` in the same directory as `install.command`.
The scenarios file will determine what scenarios are available on the web
application. Put stim images in a `stim` directory next to `install.command`,
any images here will be available to show up during scenario trials.

# Results

Results of each trial will be saved to `result.csv` in the same directory as
`install.command`. New results will be appended to the end of the file, and if
the file doesn't exist, it will be automatically generated.

# Updating

Just double-click on `install.command` or `install_windows` again to install the latest version of
the software.

# Errors

In case you get an error of something like "Port 8080 already being used when
trying to run the game, just double-click `kill_game.command` and it should
be resolved. Again, You may need to give the command permission to execute similar
to `install.command`

# Message Feature

# Run server to receive messages:
To run server on a destination computer, use the command `run_server.command` (on mac)
or `run_server_windows` (on windows)

# Update IP address of the server:
The IP address of the server is set to `100.1.1.1` by default in the file
`install.command` You may need to edit that if the server's IP address is different.
