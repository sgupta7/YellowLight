# get directory of this script and change to it
Set-Variable dir $PSScriptRoot
Set-Location $dir

$pathToModule = ".\7Zip4Powershell\1.8.0\7Zip4PowerShell.psd1"

if (-not (Get-Command Expand-7Zip -ErrorAction Ignore)) {
	Import-Module $pathToModule
}

$fileName = "$dir/archive.tar.gz"
$fileNameTar = "$dir/archive.tar"
$codeParentDir = "YellowLight-master-*"

# download, extract, and arrange the most up-to-date code

# download
$client = new-object System.Net.WebClient
$client.DownloadFile("https://gitlab.com/sgupta7/YellowLight/repository/master/archive.tar.gz", $fileName)

# extract
Expand-7Zip $fileName $dir
Expand-7Zip $fileNameTar $dir

# separate files and folders to be moved
$files = Get-ChildItem "$codeParentDir/*" -File
$folders = Get-ChildItem "$codeParentDir/*" -Directory

# Move files
Move-Item -Path $files -Destination . -Force

# Check if folders already exist and delete them
ForEach ($folder in $folders){
	$folderDest = Split-Path $folder -Leaf
	if (Test-Path $folderDest -PathType Container){
		Remove-Item $folderDest -Recurse -Force
	}
}

# Move folders
Move-Item -Path $folders -Destination . -Force

# Delete temporary files
Remove-Item $fileName
Remove-Item $fileNameTar
Remove-Item $codeParentDir -Recurse -Force

try{
	# install virtualenv
	pip install virtualenv

	# create a virtual environment and "activate" it
	python -m virtualenv venv
	venv\Scripts\activate

	# install requirements
	pip install -r requirements.txt
}
catch{
	pause
}