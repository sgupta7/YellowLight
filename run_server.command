#!/bin/sh

# get this script's directory and change to it
dir=$(dirname ${0})
cd ${dir}

# start the server
python src/server.py