#!/bin/sh

# get this script's directory and change to it
dir=$(dirname ${0})
cd ${dir}

# activate the virtual environment
source venv/bin/activate

# start the server
python src/yellow_light.py --server 172.17.28.227
