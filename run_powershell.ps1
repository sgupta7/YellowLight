# get directory of this script and change to it
Set-Variable dir $PSScriptRoot
Set-Location $dir

try{
	# activate the virtual environment
	venv\Scripts\activate

	# start the server
	python src\yellow_light.py --server 100.1.1.1
}
catch{
	pause
}