#!/bin/sh

# set the port the game is running on
PORT_NUMBER=8080

# kill the game
lsof -i tcp:${PORT_NUMBER} | awk 'NR!=1 {print $2}' | xargs kill

