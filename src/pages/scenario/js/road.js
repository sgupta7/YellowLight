define(['../../lib/easeljs-0.8.2.min', 'stage'], function(easel, stage) {

	// Declare shapes for road, sky, grass, and intersection.
	var road = new createjs.Shape(),
		sky = new createjs.Shape(),
		grass = new createjs.Shape(),
		container = new createjs.Container(),
		intersection = new createjs.Shape();

	// Define these shapes.
	road.graphics.beginFill('black')
		.moveTo(stage.canvas.width / 2, stage.canvas.height / 2)
		.lineTo(0, stage.canvas.height)
		.lineTo(stage.canvas.width, stage.canvas.height);
	sky.graphics.beginFill('lightblue')
		.drawRect(0, 0, stage.canvas.width, stage.canvas.height / 2);
	grass.graphics.beginFill('green')
		.drawRect(0, 0, stage.canvas.width, stage.canvas.height);
	intersection.graphics.beginFill('black')
		.drawRect(0, stage.canvas.height * 0.75,
			stage.canvas.width, stage.canvas.height * 0.25);

	// Add the shapes to the stage.
	stage.addChildAt(grass, 0);
	container.addChild(intersection);
	stage.addChildAt(container, 1);
	stage.addChildAt(sky, 2);
	stage.addChildAt(road, 3);

	var time, distance;

	function setTime(val) {

		// Negative ETA doesn't make sense.
		if(val < 0) {
			console.error('Invalid ETA: ' + val);
			return;
		}

		// Start the animation.
		time = val + Date.now();
		distance = 0.0;
		createjs.Ticker.addEventListener('tick', onTick);

		// Stop the animation at the end of the ETA.
		setTimeout(function() {
			createjs.Ticker.removeEventListener('tick', onTick);
		}, val);

	}

	function getTime() {
		return time;
	}

	function onTick(tick) {

		// Calculate how far the animation should jump this frame.
		// Don't go farther than 100%.
		distance += velocity() * tick.delta;
		if(distance > 1)
			distance = 1;

		// Move the animation.
		container.scaleY = distance;

		// Update the stage.
		stage.update();

	}

	/*
	 * Calculate what % of the animation to complete this frame.
	 */
	function velocity() {
		var dR = 1 - distance;
		var tR = time - Date.now();
		return dR / tR;
	}

	// Public facing API.
	return Object.create(Object.prototype, {
		time : {
			set : setTime,
			get : getTime
		}
	});

});
