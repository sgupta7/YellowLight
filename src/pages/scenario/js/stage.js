define(['../../lib/easeljs-0.8.2.min'], function(easeljs) {

	// Initialize the stage.
	var stage = new createjs.Stage("screen");

	// Set dimensions.
	stage.canvas.width = 768;
	stage.canvas.height = 576;

	// Give public access to the stage.
	return stage;

});
