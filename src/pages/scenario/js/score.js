define(['../../lib/easeljs-0.8.2.min', 'stage'], function(easel, stage) {

	var text = new createjs.Text('$0.00',
		(stage.canvas.width * 0.05) + 'px Georgia',
		'black');

	text.x = (stage.canvas.width / 2) - 40;
	text.y = 5;

	stage.addChild(text);

	var score = 0.0;

	function setScore(val) {

		score = val;

		text.text = '$' + score.toFixed(2);
		stage.update();

	}

	function getScore() {
		return score;
	}

	function setShow(val) {
		text.visible = val;
		stage.update();
	}

	function getShow() {
		return text.visible;
	}

	return Object.create(Object.prototype, {
		value : {
			set : setScore,
			get : getScore
		},
		show : {
			set : setShow,
			get : getShow
		}
	});

});
