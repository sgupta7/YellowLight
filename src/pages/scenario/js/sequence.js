define(['../../lib/jquery-3.3.1.min', 'light', 'stim', 'query',
	'choice', 'road', 'crash', 'score'],
	function(jquery, light, stim, query, choice, road, crash, score) {

	// Request scenario data, and start scenario when data arrives.
	$.get('/app/scenario/' + query.id, function(scenario) {

		// Don't start until a key is pressed.
		$(document).on('keydown', function begin() {

			$(document).off('keydown', begin);

			// Keep track of the current trial with this index number.
			var index = 0;

			// send game start signal to API
			$.get('/app/startgame');

			setTimeout(function doGreenLight() {

				/*
				 * Get the next trial and increment the index number.
				 * Stop if there are no more trials.
				 */
				var trial = scenario[index++];
				if(!trial) {
                    // send game end signal to API
                    $.get('/app/endgame');
                    return;
                }

				// Log the current trial for debug.
				console.log('Trial: ' + index);

				// Record when this trial started.
				var startT = Date.now();

				// Change light color to green.
				light.color = 'green';

				// Send trial peer feedback onset signal
                $.get('/app/log_event', {event: 'trial_start', trial_id: index});

				var totalT = (trial.greenT || 0)
					+ (trial.yellowT || 0)
					+ (trial.redT || 0);
				if(typeof trial.showScore !== 'boolean')
					trial.showScore = trial.showScore.trim().toLowerCase()
						=== 'true';
				score.show = trial.showScore;
				light.time = totalT;
				road.time = totalT;

				// Set stimulus timers.
				setTimeout(function doLeftStim() {
					stim.left = trial.leftStim;
				}, trial.leftStimT || 0);
				setTimeout(function doRightStim() {
					stim.right = trial.rightStim;
				}, trial.rightStimT || 0);

				setTimeout(function doYellowLight() {

					// Change light color to yellow.
					light.color = 'yellow';

					// Send trial yellow light onset signal
                    $.get('/app/log_event', {event: 'trial_yellow', trial_id: index});

					// Reset choice, and listen for new user choice.
					choice.stop = false;
					choice.go = false;
					choice.listen = true;

					setTimeout(function doRedLight() {

						// Change light color to red.
						light.color = 'red';

						// Send trial red light onset signal
                        $.get('/app/log_event', {event: 'trial_red', trial_id: index});

						// Stop listening for user choice.
						choice.listen = false;

						// Record when this scenario ended
						var endT = Date.now();

						/*
						 * Determine result based on if this trial has a crash
						 * and whether or not the user chose to stop or go.
						 */
						if(typeof trial.crossCar !== 'boolean') trial.crossCar
							= trial.crossCar.trim().toLowerCase() === 'true';
						var result = (trial.crossCar ?
							'crossCar' : 'noCrossCar')
							+ (choice.stop ? 'Stop' : 'Go');

						// Calculate the score based on result.
						score.value += trial[result + 'Score'] || 0;

						// Send results to API.
						$.post('/app/result', {
							scenario : query.id,
							person : query.person,
							trial : index,
							block : trial.block,
							choice : choice.string,
							result : result,
							startT : startT,
							endT : endT,
							condition : trial.condition
						});

						setTimeout(function doResult() {

							// Log result of trial for debug.
							console.log('Result: ' + result);

							// Hide stimuli.
							stim.left = undefined;
							stim.right = undefined;

							// Show result animation.
							crash.state = result;
							crash.time = trial[result + 'T'] || 0;

							// Start the loop over again.
							setTimeout(doGreenLight, trial[result + 'T'] || 0);

						}, trial.redT || 0);

					}, trial.yellowT || 0);

				}, trial.greenT || 0);

			});

		});

	});

});
