define(function() {

	var params = window.location.search.substr(1).split('&');
	if(params[0] == '')
		params = [];

	var obj = {};

	for(var i = 0; i < params.length; i++) {
		var keyval = params[i].split('=');
		obj[keyval[0]] = keyval[1];
	}

	return obj;

});
