define(['../../lib/easeljs-0.8.2.min', 'stage'], function(easel, stage) {

	var left, right,
		leftBitmap, rightBitmap;

	function setLeft(val) {
		if(!val) {
			console.log('No left stimulation.');
			stage.removeChild(leftBitmap);
			stage.update();
			left = leftBitmap = undefined;
			return;
		}
		console.log('Left stimulation: ' + val);
		left = val;
		leftBitmap = new easel.Bitmap('/app/stim/' + left);
		leftBitmap.x = 5;
		leftBitmap.y = 5;
		stage.addChild(leftBitmap);
		stage.update();
	}

	function getLeft() {
		return left;
	}

	function setRight(val) {
		if(!val) {
			console.log('No right stimulation.');
			stage.removeChild(rightBitmap);
			stage.update();
			right = rightBitmap = undefined;
			return;
		}
		console.log('Right stimulation: ' + val);
		right = val;
		rightBitmap = new createjs.Bitmap('/app/stim/' + right);
		rightBitmap.x = stage.canvas.width * 0.75;
		rightBitmap.y = 5;
		stage.addChild(rightBitmap);
		stage.update();
	}

	function getRight() {
		return right;
	}

	return Object.create(Object.prototype, {
		left : {
			set : setLeft,
			get : getLeft
		},
		right : {
			set : setRight,
			get : getRight
		}
	});

});
