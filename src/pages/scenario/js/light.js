define(['../../lib/easeljs-0.8.2.min', 'stage'], function(easel, stage) {

	// Set up the layer of animation.
	var container = new createjs.Container(),
		greenLight = new createjs.Bitmap('resources/green.png'),
		yellowLight = new createjs.Bitmap('resources/yellow.png'),
		redLight = new createjs.Bitmap('resources/red.png');
	greenLight.x = yellowLight.x = redLight.x = 150;
	greenLight.y = yellowLight.y = redLight.y = 50;
	stage.addChild(container);

	var color, time, distance;

	function setColor(val) {

		switch(val) {
			case 'green':
				container.removeAllChildren();
				container.addChild(greenLight);
				break;
			case 'yellow':
				container.removeAllChildren();
				container.addChild(yellowLight);
				break;
			case 'red':
				container.removeAllChildren();
				container.addChild(redLight);
				break;
			default:
				console.error('Invalid color: ' + val);
				return;
		}

		console.log('Light set to: ' + val);
		color = val;

	}

	function getColor() {
		return color;
	}

	function setTime(val) {

		// Negative ETA makes no sense.
		if(val < 0) {
			console.error('Invalid ETA: ' + val);
			return;
		}

		// Start the animation.
		console.log('Approaching intersection in: ' + val + 'ms');
		time = val + Date.now();
		distance = 0.0;
		createjs.Ticker.addEventListener('tick', onTick);

		// Stop the animation at the end of the ETA.
		setTimeout(function() {
			console.log('Arrived at intersection.');
			createjs.Ticker.removeEventListener('tick', onTick);
		}, val);

	}

	function getTime() {
		return time;
	}

	function onTick(tick) {

		// Calculate how far the animation should jump this frame.
		// Don't go farther than 100%.
		distance += velocity() * tick.delta;
		if(distance > 1)
			distance = 1;

		// Move the animation.
		container.scaleX = container.scaleY = distance;
		container.x = (stage.canvas.width / 2)
			- (stage.canvas.width * distance / 2);
		container.y = (stage.canvas.height / 2)
			- (stage.canvas.height * distance / 2);

		// Update the stage.
		stage.update();

	}

	/*
	 * Calculate what % of the animation to complete this frame.
	 */
	function velocity() {
		var dR = 1 - distance;
		var rT = time - Date.now();
		return dR / rT;
	}

	// Public facing API.
	return Object.create(Object.prototype, {
		color : {
			set : setColor,
			get : getColor
		},
		time : {
			set : setTime,
			get : getTime
		}
	});

});
