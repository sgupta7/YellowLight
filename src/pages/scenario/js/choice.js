define(['../../lib/jquery-3.3.1.min'], function(jquery) {

	// This will be true if this module is listening for user input.
	var listening = false;
	// This is will be true if the player has chosen to stop.
	var stop = false;
	var go = false;

	/*
	 * Handle keyDown events. Go if 3 is pressed, stop if 4 is pressed.
	 */
	function onKeyDown(event) {
		switch(event.keyCode) {
			case 51:
				setGo(true);
				setListen(false);
				break;
			case 52:
				setStop(true);
				setListen(false);
				break;
			default:
				console.error('Invalid choice: ' + event.keyCode);
		}
	}

	/*
	 * Stop listening if false and already listening, listen if true and not
	 * already listening.
	 */
	function setListen(val) {
		if(val && !listening) {
			console.log('Listening to user input.');
			$(document).on('keydown', onKeyDown);
		}
		else if(!val && listening) {
			console.log('Ignoring user input.');
			$(document).off('keydown', onKeyDown);
		}
		listening = val;
	}

	/*
	 * Return true if listening, false otherwise.
	 */
	function getListen() {
		return listening;
	}

	/*
	 * Choose to stop.
	 */
	function setStop(choice) {
		stop = choice;
		if(stop)
			console.log('Chose to stop.');
	}

	/*
	 * Return true if choosing to stop or false otherwise.
	 */
	function getStop() {
		return stop;
	}

	/*
	 * Choose to go.
	 */
	function setGo(choice) {
		go = choice;
		if(go)
			console.log('Chose to go.');
	}

	/*
	 * Return true if choosing to go or false otherwise.
	 */
	function getGo() {
		return go;
	}

	/*
	 * Get string version of choice.
	 */
	function getString() {
		return stop ? 'stop' : go ? 'go' : 'penalty';
	}

	// Public API.
	return Object.create(Object.prototype, {
		listen : {
			set : setListen,
			get : getListen
		},
		stop : {
			set : setStop,
			get : getStop
		},
		go : {
			set : setGo,
			get : getGo
		},
		string : {
			get : getString
		}
	});

});
