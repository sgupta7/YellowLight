define(['../../lib/easeljs-0.8.2.min', 'stage'], function(easel, stage) {

	var truck = new createjs.Bitmap('resources/truck.png'),
		container = new createjs.Container(),
		pow = new createjs.Bitmap('resources/pow.png');

	truck.x = 50;
	truck.y = 300;
	pow.x = 325;
	pow.y = 300;
	pow.visible = false;
	container.x = stage.canvas.width * -2;
	container.addChild(truck, pow);
	stage.addChild(container);

	var state, destX, time;

	function setState(val) {

		switch(val) {
			case 'crossCarGo':
				destX = 0;
				break;
			case 'crossCarStop':
				destX = stage.canvas.width;
				break;
			case 'noCrossCarGo':
			case 'noCrossCarStop':
				destX = stage.canvas.width * -2;
				break;
			default:
				console.error('Invalid crash state.');
		}

		state = val;

	}

	function getState(val) {
		return state;
	}

	function setTime(val) {

		// Negative ETA doesn't make sense.
		if(val < 0) {
			console.error('Invalid ETA: ' + val);
			return;
		}

		// Start the animation.
		console.log('Truck approaching destination in: ' + val + 'ms');
		time = val * 0.25 + Date.now();
		createjs.Ticker.addEventListener('tick', onTick);

		// Stop the animation at the end of the ETA.
		setTimeout(function() {
			container.x = stage.canvas.width * -2;
			createjs.Ticker.removeEventListener('tick', onTick);
		}, val);

	}

	function getTime() {
		return time;
	}

	function onTick(tick) {

		// Calculate how far to move the truck in this frame.
		// Don't go farher than destX.
		container.x += velocity() * tick.delta;
		if(container.x > destX)
			container.x = destX;
		pow.visible = container.x == destX;

		// Update the stage.
		stage.update();

	}

	function velocity() {
		var dR = destX - container.x;
		var tR = time - Date.now();
		return dR / tR;
	}

	// Public facing API.
	return Object.create(Object.prototype, {
		time : {
			set : setTime,
			get : getTime
		},
		state : {
			set : setState,
			get : getState
		}
	});

});
