requirejs.config({
	baseUrl: 'js',
	urlArgs : 'debug=' + (new Date()).getTime(), // for debugging
	shim : {
		easel : {
			exports : 'createjs'
		}
	},
	paths : {
		'jquery' : '../../../lib/jquery-3.3.1.min',
		'easel' : '../../../lib/easeljs-0.8.2.min',
		'require': '../../../lib/require'
	}
});

requirejs(['sequence', 'stage']);
