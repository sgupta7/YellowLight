# Save as client.py
# Message Sender
import os
from socket import *

host = None  # set to IP address of target computer
port = 13000

def get_server_address():
    global host, port
    if host is None:
        data = raw_input("Enter server IP address: ")
        host = data
    addr = (host, port)
    return addr


addr = get_server_address()
UDPSock = socket(AF_INET, SOCK_DGRAM)
while True:
    data = raw_input("Enter message to send or type 'exit': ")
    UDPSock.sendto(data, addr)
    if data == "exit":
        break
UDPSock.close()
os._exit(0)
