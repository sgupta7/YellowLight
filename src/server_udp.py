from socket import *
import traceback

host = None  # set to IP address of target computer
port = 13000        #using port 13000 by default
UDPSock = None
addr = None
server_udp_setup = False

default_server_ip = '100.1.1.1'


def isUDPServerSetup():
    return server_udp_setup


def initialize_socket():
    global UDPSock, server_udp_setup
    if UDPSock is None:
        UDPSock = socket(AF_INET, SOCK_DGRAM)
    server_udp_setup = True


def get_server_address(server_ip=None, reset=False, default=False):
    global host, port, default_server_ip
    if host is None or reset:
        if server_ip:
            host = server_ip
        elif default:
            host = default_server_ip
        else:
            data = raw_input("Enter server IP address: ")
            host = data
    addr = (host, port)
    initialize_socket()
    return addr


def send_message_to_server(message):
    global UDPSock
    if UDPSock is None:
        initialize_socket()
    addr = get_server_address()
    try:
        UDPSock.sendto(message, addr)
    except:
        print(traceback.print_exc())


def close_socket():
    global UDPSock
    if UDPSock is not None:
        UDPSock.close()
