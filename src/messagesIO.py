import os
import time

DELIMITER = '\t'
FILENAME = 'messages.txt'
FILE_HEADER = 'MSG{}TimeStamp{}Message_Text\n'.format(DELIMITER, DELIMITER)
FILE_ROW = str('MSG' + DELIMITER + '{}' + DELIMITER + '{}' + '\n')

MESSAGES_FILE_SETUP = False


def isMessagesFileSetup():
    global MESSAGES_FILE_SETUP
    return MESSAGES_FILE_SETUP


def initialize_messages_file(filename, reset=False):
    global FILE_HEADER, MESSAGES_FILE_SETUP, FILENAME
    if not os.path.isfile(filename) or reset:
        with open(filename, 'w') as f:
            f.write(FILE_HEADER)
    MESSAGES_FILE_SETUP = True
    FILENAME = filename


def write_message(msg):
    global FILENAME, FILE_ROW
    if not os.path.isfile(FILENAME):
        initialize_messages_file(filename=FILENAME)
    with open(FILENAME, 'a') as f:
        f.write(FILE_ROW.format(time.time(), msg))
