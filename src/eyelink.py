from pylink import *

RIGHT_EYE = 1
LEFT_EYE = 0
BINOCULAR = 2
NTRIALS = 3
TRIALDUR = 5000
SCREENWIDTH = 800
SCREENHEIGHT = 600
trial_condition = ['condition1', 'condition2', 'condition3']

tracker_setup = False


def isTrackerSetup():
    return tracker_setup


def initializeTracker(edfFileName):
    # import pdb; pdb.set_trace()
    # initialize tracker object with default IP address.
    # created objected can now be accessed through getEYELINK()
    eyelinktracker = EyeLink()
    # Here is the starting point of the experiment
    # Initializes the graphics
    # INSERT THIRD PARTY GRAPHICS INITIALIZATION HERE IF APPLICABLE
    # pylink.openGraphics((SCREENWIDTH, SCREENHEIGHT), 32)

    # Opens the EDF file.
    eyelinktracker.openDataFile(edfFileName)

    # flush all key presses and set tracker mode to offline.
    pylink.flushGetkeyQueue()
    eyelinktracker.setOfflineMode()
    msecDelay(50)

    # Sets the display coordinate system and sends mesage to that effect to EDF file;
    eyelinktracker.sendCommand("screen_pixel_coords =  0 0 %d %d" % (SCREENWIDTH - 1, SCREENHEIGHT - 1))
    eyelinktracker.sendMessage("DISPLAY_COORDS  0 0 %d %d" % (SCREENWIDTH - 1, SCREENHEIGHT - 1))

    tracker_software_ver = 0
    eyelink_ver = eyelinktracker.getTrackerVersion()
    if eyelink_ver == 3:
        tvstr = eyelinktracker.getTrackerVersionString()
        vindex = tvstr.find("EYELINK CL")
        tracker_software_ver = int(float(tvstr[(vindex + len("EYELINK CL")):].strip()))

    if eyelink_ver >= 2:
        eyelinktracker.sendCommand("select_parser_configuration 0")
        if eyelink_ver == 2:  # turn off scenelink camera stuff
            eyelinktracker.sendCommand("scene_camera_gazemap = NO")
    else:
        eyelinktracker.sendCommand("saccade_velocity_threshold = 35")
        eyelinktracker.sendCommand("saccade_acceleration_threshold = 9500")

    pylink.setCalibrationColors((0, 0, 0), (255, 255, 255))  # Sets the calibration target and background color
    # pylink.setTargetSize(SCREENWIDTH // 70, SCREENWIDTH // 300)  # select best size for calibration target
    pylink.setCalibrationSounds("", "", "")
    pylink.setDriftCorrectSounds("", "off", "off")

    # Do the tracker setup at the beginning of the experiment.
    # eyelinktracker.doTrackerSetup()

    # The Data viewer will not parse any messages, events, or samples, that exist in the data file prior to this message.
    msg = "TRIALID %d" % 1
    eyelinktracker.sendMessage(msg)
    msg = "!V TRIAL_VAR_DATA %d" % 1
    eyelinktracker.sendMessage(msg)

    global tracker_setup
    tracker_setup = True


def sendMessageToTracker(msg):
    # make sure display-tracker connection is established and no program termination or ALT-F4 or CTRL-C pressed
    eyelinktracker = getEYELINK()
    if eyelinktracker.isConnected() and not eyelinktracker.breakPressed():
        # send the message
        eyelinktracker.sendMessage("Yellowlight: {}".format(msg))


def finishTrial(edfFileName):
    eyelinktracker = getEYELINK()
    if eyelinktracker is not None:
        # File transfer and cleanup!
        eyelinktracker.setOfflineMode()
        msecDelay(500)

        # Close the file and transfer it to Display PC
        eyelinktracker.closeDataFile()
        eyelinktracker.receiveDataFile(edfFileName, edfFileName)
        eyelinktracker.close()

    # Close the experiment graphics
    pylink.closeGraphics()

