import bottle
import json
import os
from csv import DictReader, DictWriter
import sys
import traceback
import argparse
# from eyelink import *
from server_udp import *
from messagesIO import *

static_directory = 'src/pages'
stim_directory = 'stim'
scenario_file = 'scenario.csv'
result_file = 'result.csv'
edfFileName = 'yellowt1.EDF'
messages_file = 'messages.txt'

experiment_start_message = 'q'
experiment_end_message = 'p'
trial_start_message = 'Start_Trial_{}'
trial_yellowlight_onset_message = 'Trial_yellow'
trial_redlight_onset_message = 'Trial_red'

event_trial_start = 'trial_start'
event_trial_yellow = 'trial_yellow'
event_trial_red = 'trial_red'

default_server_ip = '100.1.1.1'

@bottle.get('/pages/<filepath:path>')
def server_static(filepath):
    '''
    Serve static pages. That is, pages that aren't dynamically generated.
    '''

    return bottle.static_file(filepath, root = static_directory)

@bottle.get('/app/stim/<name>')
def get_stim(name):
    '''
    Serve an image with the given name, not including file extension.
    '''

    stims = os.listdir(stim_directory)
    for stim in stims:
        if(stim.startswith(name)):
            return bottle.static_file(stim, root = stim_directory)

@bottle.get('/app/scenario/')
def get_scenarios():
    '''
    Serve a list of scenarios.
    '''

    bottle.response.content_type = 'application/json'

    ids = []
    with open(scenario_file, 'r') as scenarios:
        ids = set(scenario['id'] for scenario in DictReader(scenarios))

    return json.dumps(list(ids))

@bottle.get('/app/scenario/<pk>')
def get_scenario(pk):
    '''
    Serve a scenario for the given scenario ID number. Scenarios are read from
    a scenario CSV file.
    '''

    bottle.response.content_type = 'application/json'

    scenario = []
    with open(scenario_file, 'r') as scenarios:
        scenario = [trial for trial in DictReader(scenarios) if trial['id'] == pk]
        for trial in scenario:
            for col in trial:
                try:
                    trial[col] = json.loads(trial[col])
                except:
                    pass

    return json.dumps(scenario)

@bottle.post('/app/result')
def post_result():
    '''
    Receive the results of one trial. Results are saved to a result CSV file.
    '''

    with open(result_file, 'a') as results:

        # Use the POST keys as the CSV file header.
        writer = DictWriter(results, bottle.request.POST.keys())

        # Write the header if one doesn't exist.
        if not results.tell():
            writer.writeheader()

        # Append this POST to the end of the file.
        writer.writerow(bottle.request.POST)

@bottle.get('/app/startgame')
def start_game():
    '''
    Sends a starting game message to a server.
    '''

    global experiment_start_message
    message = experiment_start_message
    if isMessagesFileSetup():
        write_message(message)
    if isUDPServerSetup():
        send_message_to_server(message)
    # if isTrackerSetup():
    #     sendMessageToTracker(message)

@bottle.get('/app/log_event')
def log_event():
    '''
    Sends a game event message to a server.
    '''

    global trial_start_message, trial_yellowlight_onset_message, trial_redlight_onset_message
    global event_trial_start, event_trial_yellow, event_trial_red
    message = None
    event = bottle.request.query.event

    # Figure out the appropriate message to write
    if event == event_trial_start:
        trial_id = bottle.request.query.trial_id
        message = trial_start_message.format(trial_id)
    elif event == event_trial_yellow:
        message = trial_yellowlight_onset_message
    elif event == event_trial_red:
        message = trial_redlight_onset_message

    # Write/Send message
    if isMessagesFileSetup():
        write_message(message)
    if isUDPServerSetup():
        send_message_to_server(message)
    # if isTrackerSetup():
    #     sendMessageToTracker(message)

@bottle.get('/app/endgame')
def end_game():
    '''
    Sends a ending game message to a server
    '''

    global experiment_end_message, edfFileName
    message = experiment_end_message
    if isMessagesFileSetup():
        write_message(message)
    if isUDPServerSetup():
        send_message_to_server(message)
    # if isTrackerSetup():
    #     sendMessageToTracker(message)
    #     finishTrial(edfFileName)

def handle_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--server", help="The IP address of the server")
    args = vars(parser.parse_args())
    return args

args = handle_args()
initialize_messages_file(messages_file, reset=True)
# eyelink.initializeTracker(edfFileName=edfFileName)
# print("server is at: {}".format(get_server_address(reset=True, server_ip=args['server'])))
bottle.run(host='localhost', port=8080)
